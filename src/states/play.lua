-- chikun :: 2014
-- Play state


-- Temporary state, removed at end of script
local playState = { }


-- On state create
function playState:create()

    -- Those bastard enemies
    enemies = { }

    -- Static collisions for player and enemies
    collisions = { }

    -- Interpret enemies from map
    for key, value in ipairs(map.current.layers) do
        -- Interpret enemies
        if (value.name == "enemies") then
            enemy.load(value.objects)
        -- Interpret important objects
        elseif (value.name == "important") then
            for key, obj in ipairs(value.objects) do
                if obj.name == "playerStart" then
                    player:create(obj.x, obj.y, obj.w, obj.h)
                end
            end
        -- Static collisions
        elseif (value.name == "collisions") then
            for key, val in ipairs(value.objects) do
                table.insert(collisions, {
                        x = val.x,
                        y = val.y,
                        w = val.w,
                        h = val.h
                    })
            end
        end
    end

    -- Create view which follows player
    view = {
        x = 0,
        y = 0,
        w = 1920,
        h = 1080,
        xOffset = 0,
        yOffset = 0
    }

    -- Create view bubble which follows player
    viewBubble = {
        x = 0,
        y = 0,
        w = 3200,
        h = 1800,
        xOffset = 0,
        yOffset = 0
    }

end


-- On state update
function playState:update(dt)

    -- Update player
    player:update(dt)

    -- If view moveable
    if flags.viewMoveable then
        local co, si =
            math.cos(math.rad(player.direction)) * 32,
            math.sin(math.rad(player.direction)) * 32

        view.xOffset = view.xOffset + math.sign(co - view.xOffset) * 256 * dt
        if math.abs(view.xOffset - co) < 8 then
            view.xOffset = co
        end
        view.yOffset = view.yOffset + math.sign(si - view.yOffset) * 256 * dt
        if math.abs(view.yOffset - si) < 8 then
            view.yOffset = si
        end
    else
        view.xOffset = 0
        view.yOffset = 0
    end

    -- Update view based on player
    view.x = math.max(0, player.x - (view.w - player.w) / 2 + view.xOffset)
    view.y = math.max(0, player.y - (view.h - player.h) / 2 + view.yOffset - 96)

    -- Update view bubble based on player
    viewBubble.x = math.max(0, player.x - (viewBubble.w - player.w) / 2 + view.xOffset)
    viewBubble.y = math.max(0, player.y - (viewBubble.h - player.h) / 2 + view.yOffset - 96)

    -- Update enemies
    enemy.update(dt)

    -- Temp. pitches
    local pPitch, gPitch =
        (player.speed - 0.1) / 0.9 * 0.4 + 0.6,
        (flags.speed - 0.2) / 0.8 * 0.4 + 0.6

    -- Set music playback based on game speed
    lvl[level.current].bgm:setPitch(math.min(pPitch, gPitch))

end


-- On state draw
function playState:draw()

    -- Scale game due to weird shit
    g.scale(w:getWidth() / 1920, w:getHeight() / 1080)

    -- Translate view for hi-tech scrolling action
    g.translate(-view.x, -view.y)

    g.setColor(255, 255, 255)

    -- Draw map
    g.setColor(255, 255, 255)
    map.draw(map.current)

    -- Draw collisions as black
    g.setColor(0, 0, 0)
    for key, value in ipairs(collisions) do
        g.rectangle('fill', value.x, value.y, value.w, value.h)
    end

    -- Draw enemies
    enemy.draw()

    -- Actually draw the player
    player:draw()

    -- GUI

    -- Reset drawing for GUI
    g.origin()

    -- Select font used for drawing
    if (flags.lang == "zh") then
        -- Use Chinese font
        g.setFont(fnt.scoreC)
    else
        -- Use Western font
        g.setFont(fnt.scoreN)
    end

    -- Make sure score is six digits
    local score = tostring(player.score)
    while score:len() < 6 do
        score = "0" .. score
    end

    -- Determine score text based on language
    score = txt[flags.lang].score .. txt[flags.lang].punc.colon .. " " .. score

    -- Draw score shadow
    g.setColor(0, 0, 0)
    g.print(score, w:getWidth() / 10 + 17, w:getHeight() / 10 + 17)

    -- Draw score
    g.setColor(255, 255, 255)
    g.print(score, w:getWidth() / 10 + 16, w:getHeight() / 10 + 16)

    -- Select font used for drawing
    if (flags.lang == "zh") then
        -- Use Chinese font
        g.setFont(fnt.menuC)
    else
        -- Use Western font
        g.setFont(fnt.menuN)
    end

    -- MANA

    -- Local ammo object
    local ammo = {
        x = w:getWidth() * 0.9 - 116,
        y = w:getHeight() * 0.9 - 24,
        w = 102,
        h = 24,
        col = { 255, 224, 32 },
        over = player.ammoOver,
        text = txt[flags.lang].ammo:upper(),
        value = player.ammo
    }

    -- Local mana object
    local mana = {
        x = w:getWidth() * 0.9 - 116,
        y = w:getHeight() * 0.9 - 60,
        w = 102,
        h = 24,
        col = { 120, 180, 30 },
        over = player.manaOver,
        text = txt[flags.lang].mana:upper(),
        value = player.mana
    }

    -- Draw ammo and mana
    states.play:tableDraw(ammo)
    states.play:tableDraw(mana)

    -- Draw health back
    g.setColor(0, 0, 0)
    g.circle("fill", w:getWidth() / 10 + 24, w:getHeight() * 0.9 - 32, 48)

    -- Draw health insides
    local healthAlpha = 255 - math.round((player.immunity * 4) % 1) * 64
    local col1, col2 =
        math.min(50, player.health) * 2,
        math.min(50, 100 - player.health) * 2
    g.setColor(2.55 * col2, 1.27 * col1, 2.55 * col1, healthAlpha)
    local startDir = (math.pi * 1.5)
    g.arc("fill", w:getWidth() / 10 + 24, w:getHeight() * 0.9 - 32, 48,
        startDir + math.pi * 2,
        math.pi * 2 - ((math.pi * 2) * (player.health / 100)) + startDir)

    -- Draw health border
    g.setColor(255, 255, 255)
    g.draw(gfx.health, w:getWidth() / 10 - 40, w:getHeight() * 0.9 - 96)

    -- Select font used for drawing
    if (flags.lang == "zh") then
        -- Use Chinese font
        g.setFont(fnt.healthC)
    else
        -- Use Western font
        g.setFont(fnt.healthN)
    end

    -- Draw health text
    local health = tostring(player.health)
    g.setColor(0, 0, 0)
    g.printf(health, w:getWidth() / 10 + 1, w:getHeight() * 0.9 - 49, 48, 'center')
    g.setColor(255, 255, 255)
    g.printf(health, w:getWidth() / 10, w:getHeight() * 0.9 - 50, 48, 'center')

    -- Reset drawing changes
    g.origin()

end


-- On state kill
function playState:kill()

    -- Reset bullets
    bullets = { }

end




-- On table draw
function playState:tableDraw(tab)

    -- Draw tab box
    g.setColor(255, 255, 255)
    g.rectangle('fill', tab.x - 1, tab.y - 1, tab.w + 2, tab.h + 2)

    -- Draw tab skeleton
    g.setColor(0, 0, 0)
    g.rectangle('fill', tab.x, tab.y, tab.w, tab.h)

    -- Draw tab text
    g.setColor(255, 255, 255)
    g.printf(tab.text, tab.x, tab.y + 1, tab.w, 'center')

    -- Restrict tab fill drawing
    g.setScissor(tab.x, tab.y, tab.w * tab.value / 100, tab.h)

    -- Draw tab fill
    g.setColor(tab.col)
    if tab.over then
        g.setColor(180, 60, 30)
    end
    g.rectangle('fill', tab.x, tab.y, tab.w, tab.h)

    -- Draw tab text
    g.setColor(0, 0, 0)
    g.printf(tab.text, tab.x, tab.y + 1, tab.w, 'center')

    -- Release drawing restrictions
    g.setScissor()

end


-- On button press
function playState:keypressed(key)

    if key == joy.guide then
        states.pause:create()
        state.current = states.pause
    end

end


-- Transfer data to state loading script
return playState
