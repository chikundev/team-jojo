-- chikun :: 2014
-- Loads all fonts from the /fnt folder


-- Can't do this recursively due to sizes
fnt = {
    -- Splash screen font
    splash  = love.graphics.newFont("fnt/exo2.otf", 128),
    -- Chinese screen font
    zplash  = love.graphics.newFont("fnt/wqy-microhei.ttf", 128),
    -- Subtext font
    subtext = love.graphics.newFont("fnt/exo2.otf", 16),
    -- Chinese menu font
    menuC   = love.graphics.newFont("fnt/wqy-microhei.ttf", 20),
    -- Other language menu font
    menuN   = love.graphics.newFont("fnt/youth.ttf", 20),
    -- Chinese health font
    healthC  = love.graphics.newFont("fnt/wqy-microhei.ttf", 32),
    -- Other language health font
    healthN  = love.graphics.newFont("fnt/youth.ttf", 32),
    -- Chinese score font
    scoreC   = love.graphics.newFont("fnt/wqy-microhei.ttf", 48),
    -- Other language score font
    scoreN   = love.graphics.newFont("fnt/youth.ttf", 48)
}
