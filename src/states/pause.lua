-- chikun :: 2014
-- Paused state


-- Temporary state, removed at end of script
local pauseState = { }


-- On state create
function pauseState:create()

    -- Pause BGM
    lvl[level.current].bgm:pause()

    -- Play pause SFX
    sfx.pause:play()

    -- Get font to draw with
    local fntat = getPromptFont()

    -- Menu vars
    menuVars = {

        buttons = {
            {
                text = txt[flags.lang].continue,
                timer = 0
            },
            {
                text = txt[flags.lang].mainMenu,
                timer = 0
            },
            {
                text = txt[flags.lang].menu.quit,
                timer = 0
            }
        },

        prompts = {
            {
                text = txt[langs[langs.current]].choose,
                image = gfx.buttons.dpud,
                width = 56 + fntat:getWidth(txt[langs[langs.current]].choose .. " / ")
            },
            {
                text = txt[langs[langs.current]].accept,
                image = gfx.buttons.o,
                width = 56 + fntat:getWidth(txt[langs[langs.current]].accept)
            }
        },

        selected = 1

    }

end


-- On state update
function pauseState:update(dt)

    -- Work with buttons
    for key, value in ipairs(menuVars.buttons) do
        if menuVars.selected == key then
            value.timer = value.timer + 2560 * dt
            if value.timer > 356 then
                value.timer = 356
            end
        else
            value.timer = value.timer - 2560 * dt
            if value.timer < 0 then
                value.timer = 0
            end
        end
    end

end


-- On state draw
function pauseState:draw()

    -- Draw play screen
    states.play:draw()

    -- Draw fade background
    g.setColor(0, 0, 0, 192)
    g.rectangle('fill', 0, 0, w:getWidth(), w:getHeight())

    -- Draw at an offset, depending on window size
    g.translate(math.floor((w.getWidth() - 1280) / 2),
        math.floor((w.getHeight() - 720) / 2))

    -- Select font used for drawing
    if (flags.lang == "zh") then
        -- Use Chinese font
        g.setFont(fnt.scoreC)
    else
        -- Use Western font
        g.setFont(fnt.scoreN)
    end

    -- Paused text
    g.setColor(255, 255, 255)
    local pausedText = string.upper(txt[flags.lang].paused)
    g.printf(pausedText, 0, 256, 1280, 'center')

    -- Draw buttons
    buttonDraw(1, 640, 390)
    buttonDraw(2, 640, 440)
    buttonDraw(3, 640, 490)

    -- Reset drawing scaling
    g.origin()

    -- Draw button prompts
    promptDraw()

end


-- On state kill
function pauseState:kill()

end


-- On keypressed
function pauseState:keypressed(key)

    if key == joy.dd then
        menuVars.selected = menuVars.selected + 1
        if menuVars.selected > #menuVars.buttons then
            menuVars.selected = 1
        end
        -- Play move SFX
        sfx.menuMove:stop()
        sfx.menuMove:play()
    elseif key == joy.du then
        menuVars.selected = menuVars.selected - 1
        if menuVars.selected < 1 then
            menuVars.selected = #menuVars.buttons
        end
        -- Play move SFX
        sfx.menuMove:stop()
        sfx.menuMove:play()
    elseif key == joy.a then
        sfx.confirm:play()
        if menuVars.selected == 1 then
            -- Exit pause menu
            states.pause:keypressed(joy.guide)
        elseif menuVars.selected == 2 then
            -- Main menu
            state.change(states.menu)
        elseif menuVars.selected == 3 then
            -- Quit state
            state.load(states.pauseConfirm)
        end
    elseif key == joy.guide then

        -- Restart
        lvl[level.current].bgm:play()

        -- Play unpause SFX
        sfx.unpause:play()

        -- Change back to play state
        state.current = states.play
    end

end


-- Transfer data to state loading script
return pauseState
