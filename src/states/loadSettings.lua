-- chikun :: 2014
-- Load settings state


-- Temporary state, removed at end of script
local setState = { }


-- On state create
function setState:create()

    -- Load configuration file if we can
    if f.exists("config.lua") then
        readSettings()
    else
        writeSettings()
    end

    -- Move to language menu after loading
    state.change(states.language)

end


-- On state update
function setState:update(dt)

end


-- On state draw
function setState:draw()

end


-- On state kill
function setState:kill()

end


-- Read settings
function readSettings()

    -- Open configuration file
    local newFile, errorStr = f.newFile("config.lua", "r")

    -- Will hold our values!
    local values = { }

    -- Put all values from file in a table
    for value in newFile:lines() do
        table.insert(values, value)
    end

    -- Load settings with failsafes
    flags.lang          = (values[1] or "en")
    flags.volBGM        = (tonumber(values[2]) or 1)
    flags.volSFX        = (tonumber(values[3]) or 1)
    local fullscreen    = (values[4] or "0")
    if fullscreen == "1" then
        w.setFullscreen(true, "desktop")
    end

    -- Set the volume for all sounds
    setSndVolume(flags.volBGM, bgm)
    setSndVolume(flags.volSFX, sfx)

    -- Set title
    w.setTitle(txt[flags.lang].game)

    -- Close the file we opened
    newFile:close()

end


-- Write settings
function writeSettings()

    -- Start writing to the configuration file
    local newFile, errorStr = f.newFile("config.lua", "w")

    -- Write all our variables to the configuration file
    newFile:write(flags.lang .. "\r\n")
    newFile:write(flags.volBGM .. "\r\n")
    newFile:write(flags.volSFX .. "\r\n")
    local fullscreen = "0"
    if w.getFullscreen() then
        fullscreen = 1
    end
    newFile:write(fullscreen .. "\r\n")

    -- Close the file we opened
    newFile:close()

end


-- Transfer data to state loading script
return setState
