-- chikun :: 2014
-- Options state


-- Temporary state, removed at end of script
local optState = { }


-- On state create
function optState:create()

    -- Get font to draw with
    local fntat = getPromptFont()

    tmpMenuVars = menuVars

    menuVars = {
        selected  = 1,
        timer     = 0,

        buttons = {
            {
                text = txt[flags.lang].volBGM,
                timer = 0
            },
            {
                text = txt[flags.lang].volSFX,
                timer = 0
            },
            {
                text = txt[flags.lang].fullscreen,
                timer = 0
            },
            {
                text = txt[flags.lang].deny,
                timer = 0
            }
        },

        prompts = {
            {
                text = txt[langs[langs.current]].choose,
                image = gfx.buttons.dpud,
                width = 56 + fntat:getWidth(txt[langs[langs.current]].choose .. " / ")
            },
            {
                text = txt[langs[langs.current]].accept,
                image = gfx.buttons.o,
                width = 56 + fntat:getWidth(txt[langs[langs.current]].accept .. " / ")
            },
            {
                text = txt[langs[langs.current]].deny,
                image = gfx.buttons.a,
                width = 56 + fntat:getWidth(txt[langs[langs.current]].deny)
            }
        }
    }

    if s.getOS() == "Android" then
        table.remove(menuVars.buttons, 3)
    end

end


-- On state update
function optState:update(dt)

    -- Update starfield
    states.menuAesthetic:update(dt)

    -- Work with buttons
    for key, value in ipairs(menuVars.buttons) do
        if menuVars.selected == key then
            value.timer = value.timer + 2560 * dt
            if value.timer > 356 then
                value.timer = 356
            end
        else
            value.timer = value.timer - 2560 * dt
            if value.timer < 0 then
                value.timer = 0
            end
        end
    end

    -- Add space or not
    local addSpace = ""
    if flags.lang == "fr" or flags.lang == "de" then
        addSpace = " "
    end

    -- Update button titles
    menuVars.buttons[1].text = txt[flags.lang].volBGM ..
        txt[flags.lang].punc.colon .. " " .. math.round(flags.volBGM * 100)  .. txt[flags.lang].punc.percent
    menuVars.buttons[2].text = txt[flags.lang].volSFX ..
        txt[flags.lang].punc.colon .. " " .. math.round(flags.volSFX * 100)  .. txt[flags.lang].punc.percent
    if s.getOS() ~= "Android" then
        local fsText = txt[flags.lang].fullscreen
        if w:getFullscreen() then
            fsText = txt[flags.lang].windowed
        end
        menuVars.buttons[3].text = fsText
    end

    -- Get font to draw with
    local fntat = getPromptFont()

    if menuVars.selected < 3 then
        -- Figure out text
        local textYou = txt[langs[langs.current]].volBGM
        if menuVars.selected == 2 then
            textYou = txt[langs[langs.current]].volSFX
        end
        -- Update prompt titles
        menuVars.prompts = {
            {
                text = txt[langs[langs.current]].choose,
                image = gfx.buttons.dpud,
                width = 56 + fntat:getWidth(txt[langs[langs.current]].choose .. " / ")
            },
            {
                text = textYou,
                image = gfx.buttons.dplr,
                width = 56 + fntat:getWidth(textYou .. " / ")
            },
            {
                text = txt[langs[langs.current]].deny,
                image = gfx.buttons.a,
                width = 56 + fntat:getWidth(txt[langs[langs.current]].deny)
            }
        }
    else
        -- Update prompt titles
        menuVars.prompts = {
            {
                text = txt[langs[langs.current]].choose,
                image = gfx.buttons.dpud,
                width = 56 + fntat:getWidth(txt[langs[langs.current]].choose .. " / ")
            },
            {
                text = txt[langs[langs.current]].accept,
                image = gfx.buttons.o,
                width = 56 + fntat:getWidth(txt[langs[langs.current]].accept .. " / ")
            },
            {
                text = txt[langs[langs.current]].deny,
                image = gfx.buttons.a,
                width = 56 + fntat:getWidth(txt[langs[langs.current]].deny)
            }
        }
    end
end


-- On state draw
function optState:draw()

    -- Draw starfield
    states.menuAesthetic:draw()

    -- Set colour for drawing
    g.setColor(255, 255, 255)

    -- Draw at an offset, depending on window size
    g.translate(math.floor((w.getWidth() - 1280) / 2),
        math.floor((w.getHeight() - 720) / 2))

    -- Draw logo
    g.draw(gfx.logo[flags.lang], 640, 220, 0, 1, 1,
        math.floor(gfx.logo[flags.lang]:getWidth() / 2),
        math.floor(gfx.logo[flags.lang]:getHeight() / 2))

    -- Draw buttons
    local offset = 390 + (4 - #menuVars.buttons) * 25
    for i = 1, #menuVars.buttons do
        buttonDraw(i, 640, offset + (i - 1) * 50)
    end

    -- Reset drawing scaling
    g.origin()

    -- Draw button prompts
    promptDraw()

end


-- On state kill
function optState:kill()

    -- Kill menuVars
    menuVars = tmpMenuVars

    -- Write settings to file
    writeSettings()

end


-- On state keypress
function optState:keypressed(key)

    if key == joy.dd then
        menuVars.selected = menuVars.selected + 1
        if menuVars.selected > #menuVars.buttons then
            menuVars.selected = 1
        end
        -- Play move SFX
        sfx.menuMove:stop()
        sfx.menuMove:play()
    elseif key == joy.du then
        menuVars.selected = menuVars.selected - 1
        if menuVars.selected < 1 then
            menuVars.selected = #menuVars.buttons
        end
        -- Play move SFX
        sfx.menuMove:stop()
        sfx.menuMove:play()
    elseif key == joy.dl or key == joy.dr then
        -- Modify volumes based on what's selected
        local newValue = input.getAxis(joy.lx) * 0.1
        if menuVars.selected == 1 then
            flags.volBGM = math.clamp(0, flags.volBGM + newValue, 1)
            setSndVolume(flags.volBGM, bgm)
        elseif menuVars.selected == 2 then
            flags.volSFX = math.clamp(0, flags.volSFX + newValue, 1)
            setSndVolume(flags.volSFX, sfx)
            sfx.confirm:stop()
            sfx.confirm:play()
        end
    elseif key == joy.a then
        if menuVars.selected == 3 and s.getOS() ~= "Android" then
            sfx.confirm:play()
            -- Quit game
            w.setFullscreen(not w.getFullscreen(), "desktop")
        elseif menuVars.selected == #menuVars.buttons then
            sfx.confirm:play()
            -- Go to menu
            state.set(states.menu)
        end
    elseif key == joy.b then
        sfx.confirm:play()
        -- Go to menu
        state.set(states.menu)
    end

end


-- Transfer data to state loading script
return optState
