-- chikun :: 2014
-- Menu state


-- Temporary state, removed at end of script
local menuState = { }


-- On state create
function menuState:create()

    -- Play music if not playing
    if not bgm.menu:isPlaying() then
        bgm.menu:play()
    end

    -- Get font to draw with
    local fntat = getPromptFont()

    menuVars = {
        selected  = 1,
        timer     = 0,

        fade = 1,
        fadeDir = 1,
        nextState = nil,

        buttons = {
            {
                text = txt[flags.lang].levelSelect,
                timer = 0
            },
            {
                text = txt[flags.lang].menu.howtoplay,
                timer = 0
            },
            {
                text = txt[flags.lang].menu.options,
                timer = 0
            },
            {
                text = txt[flags.lang].menu.quit,
                timer = 0
            }
        },

        prompts = {
            {
                text = txt[langs[langs.current]].choose,
                image = gfx.buttons.dpud,
                width = 56 + fntat:getWidth(txt[langs[langs.current]].choose .. " / ")
            },
            {
                text = txt[langs[langs.current]].accept,
                image = gfx.buttons.o,
                width = 56 + fntat:getWidth(txt[langs[langs.current]].accept)
            }
        }
    }

end


-- On state update
function menuState:update(dt)

    -- Update starfield
    states.menuAesthetic:update(dt)

    if menuVars.fade == 0 then
        -- Work with buttons
        for key, value in ipairs(menuVars.buttons) do
            if menuVars.selected == key then
                value.timer = value.timer + 2560 * dt
                if value.timer > 356 then
                    value.timer = 356
                end
            else
                value.timer = value.timer - 2560 * dt
                if value.timer < 0 then
                    value.timer = 0
                end
            end
        end
    else
        menuVars.fade = menuVars.fade - dt * menuVars.fadeDir
        if menuVars.fade < 0 then
            menuVars.fade = 0
        end
    end

end


-- On state draw
function menuState:draw()

    -- Draw starfield
    states.menuAesthetic:draw()

    -- Determine fade alpha
    local alph = 255 - (menuVars.fade * 255)

    -- Set colour for drawing
    g.setColor(255, 255, 255, alph)

    -- Draw at an offset, depending on window size
    g.translate(math.floor((w.getWidth() - 1280) / 2),
        math.floor((w.getHeight() - 720) / 2))

    -- Draw logo
    g.draw(gfx.logo[flags.lang], 640, 220, 0, 1, 1,
        math.floor(gfx.logo[flags.lang]:getWidth() / 2),
        math.floor(gfx.logo[flags.lang]:getHeight() / 2))

    -- Draw buttons
    buttonDraw(1, 640, 390, alph)
    buttonDraw(2, 640, 440, alph)
    buttonDraw(3, 640, 490, alph)
    buttonDraw(4, 640, 540, alph)

    -- Reset drawing scaling
    g.origin()

    -- Draw button prompts
    promptDraw()

end


-- On state kill
function menuState:kill()

    -- Kill menuVars
    menuVars = nil

end


-- On state keypress
function menuState:keypressed(key)

    if menuVars.fade == 0 then
        if key == joy.dd then
            menuVars.selected = menuVars.selected + 1
            if menuVars.selected > #menuVars.buttons then
                menuVars.selected = 1
            end
            -- Play move SFX
            sfx.menuMove:stop()
            sfx.menuMove:play()
        elseif key == joy.du then
            menuVars.selected = menuVars.selected - 1
            if menuVars.selected < 1 then
                menuVars.selected = #menuVars.buttons
            end
            -- Play move SFX
            sfx.menuMove:stop()
            sfx.menuMove:play()
        elseif key == joy.a then
            sfx.confirm:play()
            if menuVars.selected == 1 then
                -- Go to level select
                state.load(states.levelSelect)
            elseif menuVars.selected == 3 then
                -- Quit game
                state.load(states.options)
            elseif menuVars.selected == 4 then
                -- Wait
                love.timer.sleep(0.25)
                -- Quit game
                e.quit()
            end
        end
    end

end


-- Transfer data to state loading script
return menuState
