-- chikun :: 2014
-- Core routines


core = { }      -- Create core table


function core.load()

    -- Load play state
    state.load(states.loadSettings)


    if #j.getJoysticks() > 0 then

        -- Update inputs first
        input.update()

    end

end


function core.update(dt)

    flags.pad = 1

    --[[for key, value in ipairs(j.getJoysticks()) do
        if value:getButtonCount() > 0 then
            flags.pad = key
            break
        end
    end]]

    if #j.getJoysticks() > 0 then

        input.update()

        -- Update state
        state.update(dt)
    end

end


-- Draws underlay
function core.underlay()

end


-- Draws overlay
function core.overlay()

    -- Alert player if not controller attached
    if #j.getJoysticks() == 0 then

        -- Reset drawing offsets
        g.origin()

        -- Background colour
        g.setColor(0, 0, 0, 128)

        -- Draw backdrop
        g.rectangle("fill", 0, 0, w:getWidth(), w:getHeight())

        -- Text colour
        g.setColor(255, 255, 255)

        -- Splash font, yo
        g.setFont(fnt.splash)
        if flags.lang == "zh" then
            g.setFont(fnt.zplash)
        end

        -- Select controller text based on language
        local text = txt[flags.lang].controller

        -- Get wrap
        wid, hei = g.getFont():getWrap(text, 1280)
        hei = hei * g.getFont():getHeight()

        -- DRAW DA CONTROLLA TEXT MANE
        g.printf(text, w:getWidth() / 2 - 640, w:getHeight() / 2 - hei / 2, 1280, 'center',
            0, 1, 1)

    end

    -- Use Western font
    g.setFont(fnt.menuN)

    -- Draw build type
    g.setColor(0, 0, 0)
    g.print("Alpha Build", 9, w.getHeight() - 23)
    g.setColor(255, 255, 255)
    g.print("Alpha Build", 8, w.getHeight() - 24)
end
