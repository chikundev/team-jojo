-- chikun :: 2014
-- Flags to control certain variables


-- Table of game flags
flags = {
    enableLighting  = false,
    enableScaling   = false,
    -- LANGUAGE CODES:
    --[[ English = en
         German  = de
         Chinese = zh
         French  = fr ]]
    lang = "en",
    gravity = 4096,
    speed = 1,
    pad = 1,
    volBGM = 1,
    volSFX = 1
}
