-- chikun :: 2014
-- Paused quit state


-- Temporary state, removed at end of script
local pauseState = { }


-- On state create
function pauseState:create()

    -- Take pause menuVars
    tmpMenuVars = menuVars

    -- Menu vars
    menuVars = {
        buttons = {
            {
                text = txt[flags.lang].deny,
                timer = 0
            },
            {
                text = txt[flags.lang].accept,
                timer = 0
            }
        },

        prompts = menuVars.prompts,

        selected = 1
    }

end


-- On state update
function pauseState:update(dt)

    -- Work with buttons
    for key, value in ipairs(menuVars.buttons) do
        if menuVars.selected == key then
            value.timer = value.timer + 2560 * dt
            if value.timer > 356 then
                value.timer = 356
            end
        else
            value.timer = value.timer - 2560 * dt
            if value.timer < 0 then
                value.timer = 0
            end
        end
    end

end


-- On state draw
function pauseState:draw()

    -- Draw play screen
    states.play:draw()

    -- Draw fade background
    g.setColor(0, 0, 0, 192)
    g.rectangle('fill', 0, 0, w:getWidth(), w:getHeight())

    -- Draw at an offset, depending on window size
    g.translate(math.floor((w.getWidth() - 1280) / 2),
        math.floor((w.getHeight() - 720) / 2))

    -- Select font used for drawing
    if (flags.lang == "zh") then
        -- Use Chinese font
        g.setFont(fnt.scoreC)
    else
        -- Use Western font
        g.setFont(fnt.scoreN)
    end

    -- Paused text
    g.setColor(255, 255, 255)
    local pausedText = string.upper(txt[flags.lang].menu.quit) .. txt[flags.lang].punc.question
    g.printf(pausedText, 0, 256, 1280, 'center')

    -- Draw buttons
    buttonDraw(1, 640, 415)
    buttonDraw(2, 640, 465)

    -- Reset drawing scaling
    g.origin()

    -- Draw button prompts
    promptDraw()

end


-- On state kill
function pauseState:kill()

    -- Reset menuVars
    menuVars = tmpMenuVars

end


-- On keypressed
function pauseState:keypressed(key)

    if key == joy.dd then
        menuVars.selected = menuVars.selected + 1
        if menuVars.selected > #menuVars.buttons then
            menuVars.selected = 1
        end
        -- Play move SFX
        sfx.menuMove:stop()
        sfx.menuMove:play()
    elseif key == joy.du then
        menuVars.selected = menuVars.selected - 1
        if menuVars.selected < 1 then
            menuVars.selected = #menuVars.buttons
        end
        -- Play move SFX
        sfx.menuMove:stop()
        sfx.menuMove:play()
    elseif key == joy.a then
        sfx.confirm:play()
        if menuVars.selected == 1 then
            -- Exit pause menu
            state.set(states.pause)
        elseif menuVars.selected == 2 then
            -- Wait
            love.timer.sleep(0.25)
            -- Quit game
            e.quit()
        end
    end

end


-- Transfer data to state loading script
return pauseState
