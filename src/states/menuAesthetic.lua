-- chikun :: 2014
-- Menu aesthetics


-- Temporary state, removed at end of script
local newState = { }


-- On state create
function newState:create()

    -- Create starfield
    dots = { }
    for i = 1, 500 do
        dots[i] = {
            x = math.random(1280),
            y = math.random(720),
            speed = 32 + math.random(8),
            direction = 5 + math.random(5)
        }
    end

end


-- On state update
function newState:update(dt)

    -- Update starfield
    for key, value in ipairs(dots) do

        -- Calculate movement direction
        local co, si
        co = math.cos(value.direction * math.pi / 180)
        si = math.sin(value.direction * math.pi / 180)

        -- Move stars
        value.x = value.x + co * value.speed * dt
        value.y = value.y - si * value.speed * dt

        -- Wrap stars around when they go off screen
        if value.x >= 1280 then
            value.x = value.x - 1280
        end
        if value.y <= 0 then
            value.y = value.y + 720
        end

    end

end


-- On state draw
function newState:draw()

    g.origin()

    -- Set colour for drawing
    g.setColor(255, 255, 255)

    -- Draw starfield
    for key, value in ipairs(dots) do
        g.rectangle("fill", value.x * w.getWidth() / 1280,
            value.y * w.getHeight() / 720, 1, 1)
    end

end


-- On state kill
function newState:kill()

    -- Stop menu music
    bgm.menu:pause()

    -- Kill starfield
    dots = nil

end


-- Draw button
function buttonDraw(num, x, y, alpha)

    -- Make sure alpha isn't nil
    alpha = alpha or 255

    -- Select font used for drawing
    if (flags.lang == "zh") then
        -- Use Chinese font
        g.setFont(fnt.menuC)
    else
        -- Use Western font
        g.setFont(fnt.menuN)
    end

    -- Important variables
    local currentButton = menuVars.buttons[num]
    local bWidth, bHeight, fWidth, fHeight =
        math.floor(gfx.menu.button:getWidth() / 2),
        math.floor(gfx.menu.button:getHeight() / 2),
        math.floor(g.getFont():getWidth(currentButton.text) / 2),
        math.floor(g.getFont():getHeight() / 2)
    local wX, wY =
        math.floor((w.getWidth() - 1280) / 2),
        math.floor((w.getHeight() - 720) / 2)

    -- Draw button
    g.setColor(255, 255, 255, alpha)
    g.draw(gfx.menu.button, x, y, 0, 1, 1, bWidth, bHeight)

    -- Restrict drawing to only on button
    g.setScissor(wX + x - bWidth + 1, wY + y - bHeight + 1,
        bWidth * 2 - 2, bHeight * 2 - 2)

    g.setBlendMode("multiplicative")

    -- Draw shine on button
    g.draw(gfx.menu.buttonSlide, x + bWidth - currentButton.timer - 4, y,
        0, 1, 1, 0, bHeight)

    g.setBlendMode("alpha")

    -- Draw text
    g.setColor(0, 0, 0, alpha)
    g.print(currentButton.text, x + 1, y + 1, 0, 1, 1, fWidth, fHeight)
    g.setColor(255, 255, 255, alpha)
    g.print(currentButton.text, x, y, 0, 1, 1, fWidth, fHeight)

    -- Reset scissor
    g.setScissor()

end


-- Draw button prompts
function promptDraw()

    local wiwi = -8
    for key, value in ipairs(menuVars.prompts) do
        wiwi = wiwi + value.width + 8
    end

    local x, y, h =
        (w:getWidth() - wiwi) / 2,
        w:getHeight() - 96,
        g.getFont():getHeight() / 2
    for key, value in ipairs(menuVars.prompts) do
        g.draw(value.image, x, y, 0, 1, 1, 0, 24)
        g.print(value.text, x + 56, y, 0, 1, 1, 0, h)
        x = x + value.width
    end

end


-- Get prompt font
function getPromptFont()

    local fntat = fnt.menuN
    -- Select font used for drawing
    if (flags.lang == "zh") then
        -- Use Chinese font
        fntat = fnt.menuC
    end

    return fntat

end


-- Transfer data to state loading script
return newState
