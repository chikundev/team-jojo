-- chikun :: 2014
-- Various language texts


txt = {
    en = {
        game = "The Temporal Sands",
        lang = "English",
        menu = {
            start       = "Start Game",
            howtoplay   = "How to Play",
            options     = "Options",
            quit        = "Quit Game"
        },
        lvl = {
            tutorial    = "Tutorial",
            caves       = "Level 1: Cave Systems",
            oldWest     = "Level 2: Old West",
            industrial  = "Level 3: Industrial",
            oriental    = "Level 4: The Orient",
            future      = "Level 5: The Future"
        },
        accept      = "Accept",
        ammo        = "Ammo",
        choose      = "Choose",
        continue    = "Continue",
        controller  = "Please connect a controller",
        deny        = "Back",
        fullscreen  = "Fullscreen",
        language    = "Language",
        levelSelect = "Level Select",
        mana        = "Mana",
        mainMenu    = "Main Menu",
        pause       = "Pause",
        paused      = "Paused",
        score       = "Score",
        volBGM      = "BGM volume",
        volSFX      = "SFX volume",
        weapon      = "Weapon",
        windowed    = "Windowed",
        punc = {
            colon       = ":",
            percent     = "%",
            question    = "?"
        }
    },
    de = {
        game = "Die Zeitlische Sands",
        lang = "Deutsch",
        menu = {
            start       = "Anfangen",
            howtoplay   = "Wie zu spielen",
            options     = "Options",
            quit        = "Beenden"
        },
        lvl = {
            tutorial    = "Tutorial",
            caves       = "Level 1: Cave Systems",
            oldWest     = "Level 2: Old West",
            industrial  = "Level 3: Industrial",
            oriental    = "Level 4: The Orient",
            future      = "Level 5: The Future"
        },
        accept      = "Wählen",
        ammo        = "Munition",
        choose      = "Aussuchen",
        continue    = "Fortsetzen",
        controller  = "Verbinden Sie bitte einen Controller",
        deny        = "Zurücksetzen",
        fullscreen  = "Vollbild",
        language    = "Sprache",
        levelSelect = "Abschnitt Auswahl",
        mana        = "Mana",
        mainMenu    = "Hauptmenü",
        pause       = "Pausieren",
        paused      = "Pausiert",
        score       = "Punkte",
        volBGM      = "Musiklaut",
        volSFX      = "Soundeffektlaut",
        weapon      = "Waffe",
        windowed    = "Fensterbild",
        punc = {
            colon       = ":",
            percent     = " %",
            question    = "?"
        }
    },
    fr = {
        game = "Les Sables Temporelles",
        lang = "Français",
        menu = {
            start       = "Commencer",
            howtoplay   = "Comment Jouer",
            options     = "Options",
            quit        = "Quitter"
        },
        lvl = {
            tutorial    = "Tutorial",
            caves       = "Level 1: Cave Systems",
            oldWest     = "Level 2: Old West",
            industrial  = "Level 3: Industrial",
            oriental    = "Level 4: The Orient",
            future      = "Level 5: The Future"
        },
        accept      = "Accepter",
        ammo        = "Munitions",
        choose      = "Choisir",
        continue    = "Continuer",
        controller  = "Connecter un contrôleur s'il vous plaît",
        deny        = "Revenir",
        fullscreen  = "Plein écran",
        language    = "Langue",
        levelSelect = "Zone sélectionner",
        mana        = "Mana",
        mainMenu    = "Menu principal",
        pause       = "Arrêter",
        paused      = "Arrêté",
        score       = "Score",
        volBGM      = "Volume de la musique",
        volSFX      = "Volume des effets",
        weapon      = "Arme",
        windowed    = "Fenêtré écran",
        punc = {
            colon       = ":",
            percent     = " %",
            question    = " ?"
        }
    },
    zh = {
        game = "颞的沙子",
        lang = "中文",
        menu = {
            start       = "开始",
            howtoplay   = "如何发挥",
            options     = "选项",
            quit        = "退出"
        },
        lvl = {
            tutorial    = "Tutorial",
            caves       = "Level 1: Cave Systems",
            oldWest     = "Level 2: Old West",
            industrial  = "Level 3: Industrial",
            oriental    = "Level 4: The Orient",
            future      = "Level 5: The Future"
        },
        accept      = "确定",
        ammo        = "弹药",
        choose      = "选择",
        continue    = "继续",
        controller  = "请插入控制器",
        deny        = "取消",
        fullscreen  = "全屏",
        language    = "语言",
        levelSelect = "阶段",
        mana        = "法力",
        mainMenu    = "主菜单",
        pause       = "暂停",
        paused      = "暂停",
        score       = "比分",
        volBGM      = "音乐音量",
        volSFX      = "音效音量",
        weapon      = "武器",
        windowed    = "窗口",
        punc = {
            colon       = "：",
            percent     = "%",
            question    = "？"
        }
    }
}
