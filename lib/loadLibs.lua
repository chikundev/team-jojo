-- chikun :: 2014
-- Load required libraries


-- Load physics
hc = require "lib/hc"

-- Load flags
require "src/flags"

-- Load lighting if enabled
if flags.enableLighting then
    require "lib/light/light"
    require "lib/light/postshader"
end

-- Load shorthand bindings
require "lib/bindings"

-- Load game text
require "src/text"

-- Load maths functions
require "lib/maths"

-- Load miscellaneous functions
require "lib/misc"

-- Load resources
require "lib/load/fonts"    -- Load fonts       NOT recursive
require "lib/load/gfx"      -- Load graphics    recursive
require "lib/load/maps"     -- Load all maps    recursive
require "lib/load/snd"      -- Load sounds      recursive
require "lib/load/states"   -- Load states      recursive

-- Load scaling functions
require "lib/scaling"

-- Load state manager
require "lib/stateManager"

-- Load input manager, depending on OS
if s.getOS() == "Android" then
    require "lib/input/ouya"
else
    require "lib/input/desktop"
end

-- Load player functions
require "src/player"

-- Load enemy functions
require "src/enemies"

-- Load levels
require "src/levels"

-- Load weapons
require "src/weapons"
