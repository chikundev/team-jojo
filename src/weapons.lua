-- chikun :: 2014
-- Weapons handler


-- Weapons function table
weapon = {
    current = "smg"
}


-- List of weapons
weapons = {
    emp = {
        bullet  = gfx.weapons.smgBullet,
        damage  = 5,
        radius  = 1,
        sfx     = sfx.fireSMG,
        speed   = 1400,
        xOffset = 11,
        yOffset = 4
    },
    laser = {
        bullet  = gfx.weapons.laserBullet,
        damage  = 5,
        radius  = 4,
        sfx     = sfx.fireLaser,
        speed   = 1400,
        xOffset = 28,
        yOffset = 16
    },
    shotgun = {
        bullet  = gfx.weapons.smgBullet,
        damage  = 5,
        radius  = 1,
        sfx     = sfx.fireSMG,
        speed   = 1400,
        xOffset = 11,
        yOffset = 4
    },
    smg = {
        bullet  = gfx.weapons.smgBullet,
        damage  = 5,
        radius  = 1,
        sfx     = sfx.fireSMG,
        speed   = 1400,
        xOffset = 11,
        yOffset = 4
    },
    sword = {
        bullet  = gfx.weapons.smgBullet,
        damage  = 5,
        radius  = 1,
        sfx     = sfx.fireSMG,
        speed   = 1400,
        xOffset = 11,
        yOffset = 4
    }
}


-- List of bullets
bullets = { }

-- Update bullets
function weapon:update(dt)

    -- Slow sounds for enemmies
    weapons["laser"].sfx:setPitch((flags.speed - 0.1) / 0.9 * 0.4 + 0.6)

    -- Slow sounds for players
    weapons["emp"].sfx:setPitch((player.speed - 0.1) / 0.9 * 0.4 + 0.6)
    weapons["shotgun"].sfx:setPitch((player.speed - 0.1) / 0.9 * 0.4 + 0.6)
    weapons["smg"].sfx:setPitch((player.speed - 0.1) / 0.9 * 0.4 + 0.6)
    weapons["sword"].sfx:setPitch((player.speed - 0.1) / 0.9 * 0.4 + 0.6)

    -- Restart?
    local restart = false

    -- Iterate backwards through bullets if can
    for i = #bullets, 1, -1 do
        -- Kill self?
        local killSelf = false

        -- Determine bullet
        local value = bullets[i]

        -- Determine speed
        local co, si =
            math.cos(value.dir),
            math.sin(value.dir)

        local deet = dt
        if value.owner ~= "player" then
            deet = dt / player.speed * flags.speed
        end

        -- Move bullet
        value.x = value.x + (value.speed * deet * co)
        value.y = value.y + (value.speed * deet * si)

        -- Collision bullet
        local colBullet = {
            x = value.x - value.radius,
            y = value.y - value.radius,
            w = value.radius * 2,
            h = value.radius * 2,
            owner = value.owner
        }

        local col = false

        for key, val in ipairs(collisions) do
            if math.overlap(val, colBullet) and val.w ~= 0 and val.h ~=0 then
                col = true
            end
        end

        -- Manage collisions
        if col then
            killSelf = true
        elseif math.overlap(colBullet, player) then
            player.damage(10)
        else
            local delEnemy = 0
            for key, enemy in ipairs(enemies) do
                if math.overlap(enemy, colBullet) and colBullet.owner == "player" and
                    enemy.activateState == "on" and not enemy.fixture then
                    delEnemy = key
                end
            end
            if delEnemy > 0 then
                enemies[delEnemy].activateState = "off"
                enemies[delEnemy].x = enemies[delEnemy].xStart or enemies[delEnemy].x
                enemies[delEnemy].y = enemies[delEnemy].yStart or enemies[delEnemy].y
                killSelf = true
            end
        end

        -- If off screen, remove self
        if not math.overlap(view, colBullet) then
            killSelf = true
        end

        -- Kill self if should
        if killSelf then
            table.remove(bullets, i)
        end
    end

    -- If restart, restart
    if player.health == 0 then
        state.change(states.play)
    end

end


-- Fire bullet
function weapon:fire(gun, nx, ny, direction, owner, range)

    -- Modify variables
    local newDir    = math.rad(direction or 0)
    local newOwner  = owner or "player"
    local newRange  = range or 36

    -- Create new bullet
    local newBullet = {
        damage  = weapons[gun].damage,
        dir     = newDir,
        image   = weapons[gun].bullet,
        radius  = weapons[gun].radius,
        owner   = newOwner,
        speed   = weapons[gun].speed,
        x       = nx + math.cos(newDir) * newRange,
        y       = ny + math.sin(newDir) * newRange,
        xOffset = weapons[gun].xOffset,
        yOffset = weapons[gun].yOffset
    }

    -- Collision bullet
    local colBullet = {
        x = newBullet.x - newBullet.radius,
        y = newBullet.y - newBullet.radius,
        w = newBullet.radius * 2,
        h = newBullet.radius * 2
    }

    -- Only spawn if on screen
    if math.overlap(view, colBullet) then
        -- Play sound
        weapons[gun].sfx:stop()
        weapons[gun].sfx:play()
        -- Add bullet to bullet table
        table.insert(bullets, newBullet)
    end

end


-- Draw bullets
function weapon:draw()

    -- Actually draw these god damn bullets
    g.setColor(255, 255, 255)
    for key, value in ipairs(bullets) do
        g.draw(value.image, value.x, value.y, value.dir, 1, 1,
            value.xOffset, value.yOffset)
    end

end
