-- chikun :: 2014
-- Ludum Dare 30


function love.load()

    -- Load up required libraries
    require "lib/loadLibs"

    -- Load game objects
    require "src/init"

    -- Load core functions
    core.load()

    -- Start state manager
    state.load()

end


function love.update(dt)

    -- Induce lag if below 30 FPS
    if dt > 1 / 30 then
        dt = 1 / 30
    end

    -- Update core functions
    core.update(dt)

end


function love.draw()

    -- Draw underlay
    core.underlay()

    -- Draw functions
    state.draw()

    -- Draw overlay
    core.overlay()

end


function love.keypressed(key)

    if key == "f11" then
        local _, _, flags = w.getMode()
        local f = {w.getFullscreen()}
        local wi, he = w.getDesktopDimensions(flags.display or 1)
        if not ((not f[1]) and (wi < 1280 or he < 720)) then
            w.setFullscreen(not f[1], "desktop")
        end
    elseif key == "f8" then
        setSndVolume(0.2, bgm)
    elseif key == "menu" then
        if (state.current.keypressed ~= nil) then
            state.current:keypressed("menu")
        end
    end

end


function love.gamepadpressed(jo, button)

    if j.getJoysticks()[flags.pad]:isGamepad() then
        print(button)

        if (state.current.keypressed ~= nil) then
            state.current:keypressed(button)
        end
    end

end


function love.joystickpressed(jo, button)

    if not j.getJoysticks()[flags.pad]:isGamepad() then
        print(button)

        if (state.current.keypressed ~= nil) then
            state.current:keypressed(button)
        end
    end

end


function love.joystickaxis(jo, axis, value)

end
