-- chikun :: 2014
-- Load, update and draw enemies


-- Table to hold enemy functions
enemy = { }

-- Table to hold enemies themselves
enemies = { }


-- Load enemies
function enemy.load(objects)

    -- Clear enemies table
    enemies = { }

    -- Load all enemies to said table
    for i = #objects, 1, -1 do

        -- If rat...
        if (objects[i].name == "rat") then
            table.insert(enemies, {
                x = objects[i].x + 12,
                y = objects[i].y + 8,
                w = objects[i].w - 61,
                h = objects[i].h - 22,
                xStart = objects[i].x,
                yStart = objects[i].y,
                ySpeed = 0,
                image = gfx.enemies.rat,
                name = "rat",
                speed = 90,
                animSpeed = 3.4,
                currentFrame = 1,
                frames = 2,
                activateState = "on",
                xOffset = 12,
                yOffset = 8
            })

        -- If turret...
        elseif (objects[i].name == "turret") then
            table.insert(enemies, {
                x = objects[i].x - 11,
                y = objects[i].y - 11,
                w = objects[i].w + 22,
                h = objects[i].h + 22,
                image = gfx.enemies.turret.reg,
                name = "turret",
                timer = 0.1,
                dir = 0,
                maxTimer = 0.16,
                activateState = "on"
                })

        -- If spike...
        elseif (objects[i].name == "spike") then
            table.insert(enemies, {
                x = objects[i].x - 11,
                y = objects[i].y - 10,
                w = objects[i].w + 22,
                h = objects[i].h + 21,
                image = gfx.enemies.spike.spikeReg,
                name = "spike",
                activateState = "on",
                fixture = true
                })
        end

    end

end


-- Update enemies
function enemy.update(dt)

    -- Iterate through all enemies
    for key, value in ipairs(enemies) do

        if math.overlap(value, viewBubble) then
            if value.activateState == "offscreen" then
                value.activateState = "on"
                value.x = value.xStart or value.x
                value.y = value.yStart or value.y
            end
        else
            value.activateState = "offscreen"
        end

        if value.activateState == "on" then

            -- Deal with the rat
            if value.name == "rat" then

                -- Speed up rat
                value.speed = value.speed + math.sign((value.x + value.w / 2) -
                   (player.x + player.w / 2)) * 1536 * dt * flags.speed

                -- Cap rat speed
                value.speed = math.min(math.abs(value.speed), 512) * math.sign(value.speed)

                -- Move rat
                value.x = value.x - value.speed * dt * flags.speed

                -- If collision move back
                if math.overlapTable(collisions, value) then
                    value.speed = -value.speed
                    value.x = value.x - value.speed * dt * flags.speed
                end

                -- Update ySpeed
                value.ySpeed = math.min(3072, value.ySpeed + 3072 * dt)

                -- Move rat
                value.y = value.y + value.ySpeed * dt * flags.speed

                -- If collision move back
                local sign = math.sign(value.ySpeed)
                while math.overlapTable(collisions, value) do
                    value.y = math.ceil(value.y) - sign
                    value.ySpeed = 0
                end

                -- Randomly make rat jump
                if math.random(1, 64) == 1 and value.ySpeed == 0 then
                    value.ySpeed = -1536
                end

                -- Update animation
                value.currentFrame = value.currentFrame + dt *
                    value.animSpeed * flags.speed * math.abs(value.speed) / 512
                if value.currentFrame > (value.frames + 1) then
                    value.currentFrame = value.currentFrame - value.frames
                end

                -- Cause damage if collide with player
                if math.overlap(value, player) then
                    player.damage(15)
                end

            -- Deal with the turret
            elseif value.name == "turret" then
                -- Rotate turrent
                value.dir = value.dir + 210 * dt * flags.speed
                if value.dir >= 360 then
                    value.dir = value.dir - 360
                end
                -- Fire countdown
                value.timer = value.timer - dt * flags.speed
                if value.timer < 0 then
                    value.timer = value.maxTimer
                    weapon:fire("laser", value.x + value.w / 2, value.y + value.h / 2,
                        value.dir, "enemy")
                end

            -- Deal with the spike
            elseif value.name == "spike" then
                -- Check collisions
                if math.overlap(value, player) then
                    player.damage(25)
                end
            end

        end

    end

end


-- Draw enemies
function enemy.draw()

    -- Set drawing colour
    g.setColor(255, 255, 255)

    -- Iterate through enemies
    for key, value in ipairs(enemies) do

        if value.activateState == "on" then

            -- Draw rat
            if value.name == "rat" then
                local dir = math.sign(value.speed)
                if value.speed == 0 then
                    value.dir = 1
                end
                g.draw(value.image[tostring(math.floor(value.currentFrame))],
                    value.x + value.w / 2, value.y - value.yOffset, 0,
                    dir, 1, value.w / 2 + value.xOffset, 0)

            -- Draw turret and spike
            else
                g.draw(value.image, value.x, value.y)
            end

        end

    end

end
