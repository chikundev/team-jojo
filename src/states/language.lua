-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local newState = { }


-- On state create
function newState:create()

    local fntat = getPromptFont()

    -- Create aesthetics
    states.menuAesthetic:create()

    langs = {
        current = 1,
        "en", "de", "fr", "zh"
    }

    for key, value in ipairs(langs) do
        if value == flags.lang then
            langs.current = key
        end
    end

    fade = 0
    fadeLength = 1

    menuVars = {
        prompts = {
            {
                text = "Choose",
                image = gfx.buttons.dplr,
                width = 56 + fntat:getWidth("Choose / ")
            },
            {
                text = "Select",
                image = gfx.buttons.o,
                width = 56 + fntat:getWidth("Select /")
            },
            {
                text = "Instaplay",
                image = gfx.buttons.a,
                width = 56 + fntat:getWidth("Instaplay")
            }
        }
    }

end


-- On state update
function newState:update(dt)

    -- Update starfield
    states.menuAesthetic:update(dt)

    if fade > 0 then
        fade = fade + dt
    end

    if fade > fadeLength then
        state.change(states.splash)
    end

    local fntat = getPromptFont()

    menuVars.prompts[1].text = txt[langs[langs.current]].choose
    menuVars.prompts[1].width = 56 + fntat:getWidth(menuVars.prompts[1].text .. " / ")
    menuVars.prompts[2].text = txt[langs[langs.current]].accept
    menuVars.prompts[2].width = 56 + fntat:getWidth(menuVars.prompts[2].text .. " / ")

end


-- On state draw
function newState:draw()

    -- Draw starfield
    states.menuAesthetic:draw()

    -- Find local value for fading
    local alph = 255 - (fade / fadeLength) * 255

    -- Set colour for drawing
    g.setColor(255, 255, 255, alph)

    -- Draw at an offset, depending on window size
    g.translate(math.floor((w.getWidth() - 1280) / 2),
        math.floor((w.getHeight() - 720) / 2))

    local fw, fh =
        gfx.flags.en:getWidth(), gfx.flags.en:getHeight()
    local prev, curr, afte =
        langs.current - 1, langs.current, langs.current + 1

    if prev < 1 then
        prev = prev + #langs
    end
    if afte > #langs then
        afte = afte - #langs
    end

    -- Draw flags
    g.draw(gfx.flags[langs[curr]], 640, 384, 0, 1, 1, fw / 2, fh)
    g.draw(gfx.flags[langs[afte]], 1040, 384, 0, 0.5, 0.5, fw / 2, fh)
    g.draw(gfx.flags[langs[prev]], 240, 384, 0, 0.5, 0.5, fw / 2, fh)

    local fntat = fnt.menuN
    -- Select font used for drawing
    if (langs[langs.current] == "zh") then
        -- Use Chinese font
        fntat = fnt.menuC
    end

    g.setFont(fntat)

    g.origin()

    promptDraw()

end


-- On state kill
function newState:kill()

    fade = nil
    fadeLength = nil

    writeSettings()

end


-- On state keypress
function newState:keypressed(key)

    if fade == 0 then
        if key == joy.a then
            flags.lang = langs[langs.current]
            sfx.startup:play()
            fade = 0.01
        elseif key == joy.b then
            -- Start game
            level.load(1)
        -- If pressing left
        elseif key == joy.dl or key == 9 then
            -- Select next language
            langs.current = langs.current - 1
            -- Loop if need be
            if langs.current < 1 then
                langs.current = langs.current + #langs
            end
            -- Play move SFX
            sfx.menuMove:play()
            -- Set title
            w.setTitle(txt[langs[langs.current]].game)
        -- If pressing right
        elseif key == joy.dr or key == 10 then
            -- Select next language
            langs.current = langs.current + 1
            -- Loop if need be
            if langs.current > #langs then
                langs.current = langs.current - #langs
            end
            -- Play move SFX
            sfx.menuMove:play()
            -- Set title
            w.setTitle(txt[langs[langs.current]].game)
        end
    end

end


-- Transfer data to state loading script
return newState
