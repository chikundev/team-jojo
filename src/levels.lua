-- chikun :: 2014
-- List of levels


-- Table containing levels
lvl = {

    {
        name = "Tutorial",
        map  = maps.tutorial,
        bgm  = bgm.newgate
    },
    {
        name = "Cave Systems",
        map  = maps.caves,
        bgm  = bgm.newgate
    }

}


-- Table containing level functions
level = {
    current = 0
}


-- On level load
function level.load(num)

    map.current = lvl[num].map
    lvl[num].bgm:stop()
    lvl[num].bgm:play()
    level.current = num
    state.change(states.play)

end


-- On level kill
function level.kill()

    lvl[num].bgm:stop()

end
