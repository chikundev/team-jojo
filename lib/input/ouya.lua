-- chikun :: 2014
-- Handles input from Ouya gamepad


-- The table that houses input functions
input = {
    deadzone = 0.15
}


-- Load bindings
joy = {
    lx =    1,
    ly =    2,
    rx =    3,
    ry =    4,
    du =    12,
    dd =    13,
    dl =    14,
    dr =    15,
    a =     1,
    b =     2,
    x =     3,
    y =     4,
    l1 =    10,
    r1 =    11,
    l2 =    16,
    r2 =    17,
    guide = 'menu'
}


-- Update input
function input.update()

end


-- Check axis
function input.getAxis(axis, bypassDeadzone)

    local bypass = true
    if bypassDeadzone == nil then
        bypass = false
    end

    -- Get joypad
    local returnValue, jo = 0,
        j.getJoysticks()[flags.pad]

    if jo ~= nil then
        if axis == joy.lx then
            returnValue = input.getButton(joy.dr) - input.getButton(joy.dl)
        end

        if returnValue == 0 then
            returnValue = jo:getAxis(axis)
            if (math.abs(returnValue) < input.deadzone) and (not bypass) then
                returnValue = 0
            end
        end
    end

    return returnValue

end


-- Check button
function input.getButton(button)

    -- Get joypad
    local returnValue, jo = 0,
        j.getJoysticks()[flags.pad]

    if jo ~= nil then
        if jo:isDown(button) then
            returnValue = 1
        end
    end

    return returnValue

end


-- Check L2
function input.checkL2()

    return input.getButton(joy.l2)

end


-- Check R2
function input.checkR2()

    return input.getButton(joy.r2)

end



--[[
    -- Table of key tables
joy = {
    lx =    { 'leftx' },
    ly =    { 'lefty' },
    rx =    { 'rightx' },
    ry =    { 'righty' },
    du =    { 'dpup },
    dd =    { 'dpdown' },
    dl =    { 'dpleft' },
    dr =    { 'dpright' },
    a =     { 'a' },
    b =     { 'b' },
    x =     { 'x' },
    y =     { 'y' },
    l1 =    { 'leftshoulder' },
    r1 =    { 'rightshoulder' },
    l2 =    { 'triggerleft' },
    r2 =    { 'triggerright' }
}
        ]]
