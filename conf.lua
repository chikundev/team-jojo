-- chikun :: 2014
-- Configuration file


function love.conf(game)

    -- Attach a console for Windows debug [DISABLE ON DISTRIBUTION]
    game.console    = true

    -- Identity for saving files
    game.identity   = "TTSands"

    -- Version of LÖVE which this game was made for
    game.version    = "0.9.1"

    -- Omit modules due to disuse
    game.modules.math       = false
    game.modules.mouse      = false
    game.modules.physics    = false
    game.modules.thread     = false

    -- Various window settings
    game.window.title           = "The Temporal Sands"
    game.window.fullscreen      = false
    game.window.fullscreentype  = "desktop"
    game.window.minwidth        = 1280
    game.window.minheight       = 720
    game.window.width           = 1280
    game.window.height          = 720
    game.window.resizable       = true
    game.window.icon            = "gfx/gameicon.png"
    game.window.fsaa            = 4

end
