-- chikun :: 2014
-- Level select state


-- Temporary state, removed at end of script
local selectState = { }


-- On state create
function selectState:create()

    -- Get font to draw with
    local fntat = getPromptFont()

    tmpMenuVars = menuVars

    menuVars = {
        selected  = 1,
        timer     = 0,

        buttons = {
            {
                text = txt[flags.lang].lvl.tutorial,
                timer = 0
            },
            {
                text = txt[flags.lang].lvl.caves,
                timer = 0
            },
            {
                text = txt[flags.lang].lvl.oldWest,
                timer = 0
            },
            {
                text = txt[flags.lang].lvl.industrial,
                timer = 0
            },
            {
                text = txt[flags.lang].lvl.oriental,
                timer = 0
            },
            {
                text = txt[flags.lang].lvl.future,
                timer = 0
            },
            {
                text = txt[flags.lang].deny,
                timer = 0
            }
        },

        prompts = {
            {
                text = txt[langs[langs.current]].choose,
                image = gfx.buttons.dpud,
                width = 56 + fntat:getWidth(txt[langs[langs.current]].choose .. " / ")
            },
            {
                text = txt[langs[langs.current]].accept,
                image = gfx.buttons.o,
                width = 56 + fntat:getWidth(txt[langs[langs.current]].accept .. " / ")
            },
            {
                text = txt[langs[langs.current]].deny,
                image = gfx.buttons.a,
                width = 56 + fntat:getWidth(txt[langs[langs.current]].deny)
            }
        }
    }

end


-- On state update
function selectState:update(dt)

    -- Update starfield
    states.menuAesthetic:update(dt)

    -- Work with buttons
    for key, value in ipairs(menuVars.buttons) do
        if menuVars.selected == key then
            value.timer = value.timer + 2560 * dt
            if value.timer > 356 then
                value.timer = 356
            end
        else
            value.timer = value.timer - 2560 * dt
            if value.timer < 0 then
                value.timer = 0
            end
        end
    end

end


-- On state draw
function selectState:draw()

    -- Draw starfield
    states.menuAesthetic:draw()

    -- Set colour for drawing
    g.setColor(255, 255, 255)

    -- Draw at an offset, depending on window size
    g.translate(math.floor((w.getWidth() - 1280) / 2),
        math.floor((w.getHeight() - 720) / 2))

    -- Draw buttons
    local offset = 390
    for i = 1, #menuVars.buttons do
        buttonDraw(i, 640, offset + (i - 4) * 50)
    end

    -- Reset drawing scaling
    g.origin()

    -- Draw button prompts
    promptDraw()

end


-- On state kill
function selectState:kill()

    -- Kill menuVars
    menuVars = tmpMenuVars

    -- Write settings to file
    writeSettings()

end


-- On state keypress
function selectState:keypressed(key)

    if key == joy.dd then
        menuVars.selected = menuVars.selected + 1
        if menuVars.selected > #menuVars.buttons then
            menuVars.selected = 1
        end
        -- Play move SFX
        sfx.menuMove:stop()
        sfx.menuMove:play()
    elseif key == joy.du then
        menuVars.selected = menuVars.selected - 1
        if menuVars.selected < 1 then
            menuVars.selected = #menuVars.buttons
        end
        -- Play move SFX
        sfx.menuMove:stop()
        sfx.menuMove:play()
    elseif key == joy.a then
        if menuVars.selected == #menuVars.buttons then
            sfx.confirm:play()
            -- Go to menu
            state.set(states.menu)
        else
            -- Stop music
            bgm.menu:stop()
            -- Start game
            level.load(menuVars.selected)
        end
    elseif key == joy.b then
        sfx.confirm:play()
        -- Go to menu
        state.set(states.menu)
    end

end


-- Transfer data to state loading script
return selectState
