-- chikun :: 2014
-- Splash state


-- Temporary state, removed at end of script
local splashState = { }


-- On state create
function splashState:create()

    splashVars = {
        timer   = 1,
        played  = false
    }

    -- Play menu music
    bgm.menu:play()

end


-- On state update
function splashState:update(dt)

    -- Update starfield
    states.menuAesthetic:update(dt)

    -- Decrease timer
    splashVars.timer = splashVars.timer - (dt / 3.5)

    -- If timer is empty, change state to menu
    if splashVars.timer < 0 then
        state.change(states.menu)
    elseif splashVars.timer <= 0.5 and not splashVars.played then
        splashVars.played = true
    end

end


-- On state draw
function splashState:draw()

    -- Draw starfield
    states.menuAesthetic:draw()

    -- Calculate useable alpha value
    local alpha = 1 - math.abs(splashVars.timer - 0.5) * 2

    -- Draw chikun logo in middle of screen
    g.setFont(fnt.splash)
    g.setColor(0, 0, 0, alpha * 255)
    g.printf("chikun", 1, (w:getHeight() - fnt.splash:getHeight())/2 + 1, w:getWidth(), "center")
    g.setColor(255, 255, 255, alpha * 255)
    g.printf("chikun", 0, (w:getHeight() - fnt.splash:getHeight())/2, w:getWidth(), "center")

end


-- On state kill
function splashState:kill()

    -- Kill splashVars
    splashVars = nil

end


-- Transfer data to state loading script
return splashState
