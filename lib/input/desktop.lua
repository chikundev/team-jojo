-- chikun :: 2014
-- Handles input from gamepad


-- The table that houses input functions
input = {
    deadzone = 0.15
}


function input.update()

    if j.getJoysticks()[flags.pad]:isGamepad() then
        -- Load bindings
        joy = {
            lx =    'leftx',
            ly =    'lefty',
            rx =    'rightx',
            ry =    'righty',
            du =    'dpup',
            dd =    'dpdown',
            dl =    'dpleft',
            dr =    'dpright',
            a =     'a',
            b =     'b',
            x =     'x',
            y =     'y',
            l1 =    'leftshoulder',
            r1 =    'rightshoulder',
            l2 =    'triggerleft',
            r2 =    'triggerright',
            guide = 'guide',
            alt =   false
        }
    else
        -- Load bindings
        joy = {
            lx =    1,
            ly =    2,
            rx =    3,
            ry =    4,
            du =    0,
            dd =    0,
            dl =    0,
            dr =    0,
            a =     1,
            b =     2,
            x =     3,
            y =     4,
            l1 =    5,
            r1 =    7,
            l2 =    6,
            r2 =    8,
            guide = 9,
            alt =   false
        }
        if j.getJoysticks()[flags.pad]:getName() == "Generic X-Box pad" then
            joy.rx = 4
            joy.ry = 5
            joy.alt = true
        else
            joy.a  = 2
            joy.b  = 3
            joy.x  = 1
            joy.y  = 4
            joy.l1 = 5
            joy.l2 = 7
            joy.r1 = 6
            joy.r2 = 8
            joy.alt = true
        end
    end

end


-- Check axis
function input.getAxis(axis, bypassDeadzone)

    local bypass = true
    if bypassDeadzone == nil then
        bypass = false
    end

    -- Get joypad
    local returnValue, jo = 0,
        j.getJoysticks()[flags.pad]

    if axis == joy.lx then
        returnValue = input.getButton(joy.dr) - input.getButton(joy.dl)
    end

    if returnValue == 0 then
        if jo:isGamepad() then
            returnValue = jo:getGamepadAxis(axis)
        else
            returnValue = jo:getAxis(axis)
        end
        if (math.abs(returnValue) < input.deadzone) and (not bypass) then
            returnValue = 0
        end
    end

    return returnValue

end


-- Check button
function input.getButton(button)

    -- Get joypad
    local returnValue, jo = 0,
        j.getJoysticks()[flags.pad]

    if jo:isGamepad() then
        if jo:isGamepadDown(button) then
            returnValue = 1
        end
    else
        if jo:isDown(button) then
            returnValue = 1
        end
    end

    return returnValue

end


-- Check L2
function input.checkL2()

    local returnValue = 0

    if not joy.alt then
        returnValue = input.getAxis(joy.l2)
    else
        returnValue = input.getButton(joy.l2)
    end

    return returnValue

end


-- Check R2
function input.checkR2()

    local returnValue = 0

    if not joy.alt then
        returnValue = input.getAxis(joy.r2)
    else
        returnValue = input.getButton(joy.r2)
    end

    return returnValue

end



--[[
    -- Table of key tables
joy = {
    lx =    { 'leftx' },
    ly =    { 'lefty' },
    rx =    { 'rightx' },
    ry =    { 'righty' },
    du =    { 'dpup },
    dd =    { 'dpdown' },
    dl =    { 'dpleft' },
    dr =    { 'dpright' },
    a =     { 'a' },
    b =     { 'b' },
    x =     { 'x' },
    y =     { 'y' },
    l1 =    { 'leftshoulder' },
    r1 =    { 'rightshoulder' },
    l2 =    { 'triggerleft' },
    r2 =    { 'triggerright' }
}
        ]]
