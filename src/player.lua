-- chikun :: 2014
-- Player script


-- Player table
player = { }


-- On player create
function player:create(x, y, w, h)

    -- Position the player
    player.x, player.y = x, y

    -- Size the player
    player.w, player.h = w, h

    -- Set important variables
    player.canJump   = false        -- Can the player jump?
    player.ammo      = 100          -- Percentage of ammo available
    player.ammoOver  = false        -- Has the ammo overheated?
    player.direction = 0            -- Which way is the player pointing?
    player.health    = 100          -- How much health does the player have remaining?
    player.hover     = 0            -- Sine calculated from this for player hover
    player.immunity  = 1            -- Immunity time
    player.jumpFlip  = 0            -- Direction of flip from jump
    player.mana      = 0            -- Mana that player has available
    player.manaOver  = false        -- Has mana overheated?
    player.score     = 0            -- Score that the player has
    player.speed     = 1            -- Speed of player, affected by slowdown
    player.weapon    = "smg"        -- Current weapon player is holding
    player.wTimer    = 0            -- Time between shots of weapons
    player.xDir      = 1            -- Direction in which player is pointing
    player.xSpeed    = 0            -- How fast player is moving horizontally
    player.ySpeed    = 8            -- How fast player is moving vertically
    player.xPrevious = player.x     -- Last X position of player
    player.yPrevious = player.y     -- Last Y position of player

end


-- On player update
function player:update(dt)

    -- Decrease player immunity
    player.immunity = math.max(0, player.immunity - dt)

    -- Increase player hover
    player.hover = player.hover + dt * 180
    if player.hover >= 360 then
        player.hover = player.hover - 360
    end

    -- Manage slowing
    player:manaManage(dt)

    -- Slow player based on player speed
    dt = dt * player.speed

    -- Manage ammo
    player:ammoManage(dt)

    -- Update weapons if needed
    weapon:update(dt)

    -- MOVEMENT

    -- Disable jumpability if falling
    if player.ySpeed ~= 0 then
        player.canJump = false
    end

    -- Jump player if can and want to
    if input.checkL2() > 0.65 and player.canJump then
        player.ySpeed = -1600
        player.canJump = false
        -- Flip player if adequate space above head
        local tmpPlayer = {
            x = player.x,
            y = player.y - player.h * 2,
            w = player.w,
            h = player.h * 3
        }
        if not math.overlapTable(collisions, tmpPlayer) then
            player.jumpFlip = 720 * dt
        end
    end

    -- Induce gravity
    player.ySpeed = math.min(player.ySpeed + flags.gravity * dt, 1800)
    player.y = player.y + player.ySpeed * dt
    local yDir = math.sign(player.ySpeed)
    while player:checkCollision() do
        if player.ySpeed > 0 then
            player.canJump = true
        end
        player.ySpeed = 0
        player.y = math.ceil(player.y - yDir)
    end

    -- Move player left and right
    local xMove = input.getAxis(joy.lx)
    player.x = player.x + xMove * 512 * dt
    local test = {
        x = player.x,
        y = player.y - 16,
        w = player.w,
        h = player.h
    }
    while player:checkCollision() do
        if not math.overlapTable(collisions, test) then
            player.y = player.y - 1
            test.y = player.y - 16
        else
            player.x = math.ceil(player.x - math.sign(xMove))
        end
    end
    if xMove ~= 0 then
        player.xDir = math.sign(xMove)
    end

    -- Prevent falling off sides
    if player.x < 0 then
        player.x = 0
    elseif player.x + player.w > map.current.tileW * map.current.w then
        player.x = map.current.tileW * map.current.w - player.w
    end
    -- FLIP PLAYER
    if player.jumpFlip > 0 then
        -- Flip faster if on ground
        local flipSpeed = 1
        if player.canJump then
            flipSpeed = 2.5
        end
        player.jumpFlip = player.jumpFlip + 720 * dt * flipSpeed
        if player.jumpFlip >= 360 then
            player.jumpFlip = 0
        end
    end

    -- AIMING

    -- Set player aim
    local rx, ry =
        input.getAxis(joy.rx, true), input.getAxis(joy.ry, true)
    if math.abs(rx) > 0.1 or math.abs(ry) > 0.1 then
        player.direction = math.deg(math.atan2(ry, rx))
        if player.direction < 0 then
            player.direction = player.direction + 360
        end
    end

    player.xPrevious = player.x
    player.yPrevious = player.y

end


-- On player draw
function player:draw()

    -- Change player alpha
    local alpha = 255
    if player.immunity > 0 then
        alpha = 127
    end

    -- Draw player glow
    local col = (math.abs(player.hover - 180) + math.abs(player.direction - 180) / 360)
    g.setColor(255 * col, 255 - (255 * col), 0, alpha)
    g.draw(gfx.player.glow, player.x + player.w / 2, player.y +
        math.sin(math.rad(player.hover)) * 3 + player.h / 2,
        math.rad(player.jumpFlip) * player.xDir, 1, 1, 24, 40)

    -- Draw player
    g.setColor(255, 255, 255, alpha)
    g.draw(gfx.player.body, player.x + player.w / 2, player.y +
        math.sin(math.rad(player.hover)) * 3 + player.h / 2,
        math.rad(player.jumpFlip) * player.xDir, 1, 1, player.w / 2, player.h / 2)

    -- Player aim

    -- Flip?
    local xScale, dir = 1, player.direction
    if dir <= 270 and dir > 90 then
        xScale = -1
        dir = dir + 180
    end

    g.draw(gfx.weapons.smgAim, player.x + player.w / 2, player.y + player.h / 2,
        math.rad(dir), xScale, 1, 48, 48)

    -- Draw weapons
    weapon:draw()

end


-- MANAGE THE AMMO CHARGE
function player:ammoManage(dt)

    -- Get ammo keys
    local r2 = input.checkR2() >= 0.5

    -- Recharge ammo if overcharged
    if player.ammoOver then
        player.ammo = math.min(100, player.ammo + 30 * dt)
        if player.ammo == 100 then
            player.ammoOver = false
        end
    end

    -- Detract from weapon timer
    player.wTimer = math.max(0, player.wTimer - dt)

    -- Recharge ammo if not pressing buttons and undercharged
    if not player.ammoOver and not r2 then
        player.ammo = math.min(100, player.ammo + dt * 100)
    end

    -- Slow others if can
    if not player.ammoOver and r2 and player.wTimer == 0 then
        weapon:fire("smg", player.x + player.w / 2, player.y + player.h / 2, player.direction)
        player.ammo = player.ammo - 1
        player.wTimer = 0.04 + math.random() / 50
    end

    -- If ammo below 0, overheat
    if player.ammo < 0 then
        player.ammo = 0
        player.ammoOver = true
    end

end


-- CHECK COLLISION
function player:checkCollision()

    local col = false

    local lastPos = {
        x = player.xPrevious,
        y = player.yPrevious,
        w = player.w,
        h = player.h
    }

    for key, val in ipairs(collisions) do
        if math.overlap(val, player) then
            if val.h == 0 then
                if player.ySpeed >= 0 and not math.overlap(val, lastPos) then
                    col = true
                end
            else
                col = true
            end
        end
    end

    return col

end


-- Damage the player
function player.damage(amount)

    -- Check for player immunity
    if player.immunity == 0 then

        -- Make player immune
        player.immunity = 1

        -- Damage player
        player.health = math.max(0, player.health - amount)

    end

end


-- MANAGE THE MANA
function player:manaManage(dt)

    -- Get mana keys
    local l1, r1 =
        (input.getButton(joy.l1) == 1),
        (input.getButton(joy.r1) == 1)

    -- Recharge mana if overcharged
    if player.manaOver then
        player.mana = math.min(100, player.mana + 30 * dt)
        if player.mana == 100 then
            player.manaOver = false
        end
    end

    -- Recharge mana if not pressing buttons and undercharged
    if not player.manaOver and not (l1 or r1) then
        player.mana = math.min(100, player.mana + dt * 100)
    end


    -- Slow player if can
    if not player.manaOver and l1 then
        player.speed = math.max(0.1, player.speed - dt * 10)
        player.mana = player.mana - dt * 25
    else
        player.speed = math.min(1, player.speed + dt * 10)
    end

    -- Slow others if can
    if not player.manaOver and r1 then
        flags.speed = math.max(0.2, flags.speed - dt * 10)
        player.mana = player.mana - dt * 25
    else
        flags.speed = math.min(1, flags.speed + dt * 10)
    end

    -- If mana below 0, overheat
    if player.mana < 0 then
        player.mana = 0
        player.manaOver = true
    end

end


-- On player kill
function player:kill()

end
